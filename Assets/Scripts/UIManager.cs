using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public Canvas deathScreen;

    Volume volume;
    public ColorAdjustments colorAdjustments;
    public DepthOfField depthOfField;
    public TMP_Text dialogueName;
    public TMP_Text dialogueText;
    public Animator levelTransition;
    public AnimationClip fadeAnimation;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Update()
    {
        volume = FindObjectOfType<Volume>();
        volume.profile.TryGet<ColorAdjustments>(out colorAdjustments);
        volume.profile.TryGet<DepthOfField>(out depthOfField);
    }

    public void GameOver()
    {
        if (!ScoreManager.Instance.isDead)
        {
            ScoreManager.Instance.isDead = true;
            deathScreen.gameObject.SetActive(true);
            deathScreen.GetComponent<ShowDeathScreen>().show();
        }
    }
}
