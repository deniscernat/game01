﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ShowDeathScreen : MonoBehaviour
{
    public Animator animator;
    public List<Button> buttons = new List<Button>();

    Volume volume;
    ColorAdjustments colorAdjustments;
    DepthOfField depthOfField;

    private void FixedUpdate()
    {
        colorAdjustments = FindObjectOfType<UIManager>().colorAdjustments;
        depthOfField = FindObjectOfType<UIManager>().depthOfField;

        if (ScoreManager.Instance.isDead)
        {
            colorAdjustments.saturation.value -= 2;
            depthOfField.focusDistance.value -= 0.03f;
            if (depthOfField.focusDistance.value <= 0.9f)
                depthOfField.focusDistance.value = 0.9f;
        }
        else
        {
            colorAdjustments.saturation.value = 0;
            depthOfField.focusDistance.value = 3.5f;
        }
    }

    public void show()
    {
        animator.SetBool("Show", true);
        foreach(Button button in buttons)
        {
            button.interactable = true;
        }
    }

    public void hide()
    {
        animator.SetBool("Show", false);
        foreach (Button button in buttons)
        {
            button.interactable = false;
        }
    }
}
