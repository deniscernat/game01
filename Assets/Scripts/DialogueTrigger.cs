using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool triggered;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision & collision.tag == "Player" && !triggered)
        {
            triggered = true;
            if (!PhoneManager.Instance.phoneWasCalled && dialogue.phoneCall)
            {
                PhoneManager.Instance.phoneWasCalled = true;
                PhoneManager.Instance.phoneUp = true;
                PhoneManager.Instance.calling = true;
            }
            FindObjectOfType<DialogueManager>().dialogues.Add(dialogue);

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "_Popup");
    }
}
