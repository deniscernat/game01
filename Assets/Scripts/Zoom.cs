﻿using UnityEngine;

public class Zoom : MonoBehaviour
{
    public float scrollSpeed;
    public float zoomSpeed;
    public float minZoom, maxZoom;
    private Camera cam;
    private float distance;

    private void Start()
    {
        cam = Camera.main;
        distance = cam.orthographicSize;
    }

    void Update()
    {
        if (!ScoreManager.Instance.isDead)
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distance, zoomSpeed);
    }

    public void ZoomIn()
    {
        distance = maxZoom;
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distance, zoomSpeed);
    }

    public void ZoomOut()
    { 
        distance = minZoom;
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distance, zoomSpeed);
    }

    public void ZoomTo(float customDistance)
    {
        cam = Camera.main;
        distance = customDistance;
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distance, zoomSpeed);
    }
}
    