 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LoadData
{
    public static string levelToLoad;

    public static void Load()
    {
        try
        {
            levelToLoad = ES2.Load<string>("savedScene");
            Debug.Log("Scene found: " + levelToLoad);
        } catch
        {
            Debug.Log("Scene not found");
            levelToLoad = "0";
        }
        Loader.Load(levelToLoad);


        ScoreManager.Instance.coins = ES2.Load<int>("coins");
        ScoreManager.Instance.currentHealth = ES2.Load<float>("currentHealth");
        ScoreManager.Instance.grades = ES2.LoadList<int>("grades");
        ScoreManager.Instance.activeCharacterIndex = ES2.Load<int>("activeCharacterIndex");
        ScoreManager.Instance.backpackPickedUp = ES2.Load<bool>("backpackPickedUp");
        ScoreManager.Instance.bowPickedUp = ES2.Load<bool>("bowPickedUp");

    }

    public static void NewGame()
    {
        levelToLoad = "0";
        Loader.Load(levelToLoad);
    }
}
