﻿using UnityEngine;

// Makes objects float up & down while gently spinning.
public class Floater : MonoBehaviour
{
    public float amplitude = 0.5f;
    public float frequency = 1f;
    public bool conditional;
    public bool activated;


    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();
    Vector3 initialPos = new Vector3();

    // Use this for initialization
    void Start()
    {
        // Store the starting position & rotation of the object
        posOffset = transform.position;
        initialPos = transform.position;
    }

    void Update()
    {
        if (conditional)
        {
            if (activated)
            {
                tempPos = posOffset;
                tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
                transform.position = new Vector3(transform.position.x, tempPos.y, transform.position.z);
            } else
            {
                transform.position = Vector3.MoveTowards(transform.position, initialPos, amplitude / 4);
                posOffset = transform.position;
            }
        } else
        {
            tempPos = posOffset;
            tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
            transform.position = new Vector3(transform.position.x, tempPos.y, transform.position.z);
        }



    }
}
