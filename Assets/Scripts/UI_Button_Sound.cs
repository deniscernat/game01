using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_Button_Sound : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    public bool onDeathScreen;
    public bool onPause;
    public string soundOnHover = "ui_button_hover";
    public string soundOnClick = "ui_button_click_1";

    public void OnPointerEnter(PointerEventData ped)
    {
        if (GetComponent<Button>().enabled && GetComponent<Button>().interactable)
        {
            if (onDeathScreen && !ScoreManager.Instance.isDead) return;
            FindObjectOfType<UIAudioManager>().Play(soundOnHover);
        }
    }

    public void OnPointerDown(PointerEventData ped)
    {
        if (GetComponent<Button>().enabled && GetComponent<Button>().interactable)
            FindObjectOfType<UIAudioManager>().Play(soundOnClick);
    }
}
