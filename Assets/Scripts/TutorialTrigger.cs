using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    public Tutorial tutorial;
    public float delay;
    bool triggered;
    bool pressed;
    bool played;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision & collision.tag == "Player" && !triggered)
            triggered = true;
    }

    private void Update()
    {
        if (tutorial.keys.Length > 0 && triggered)
        {
            foreach (KeyCode key in tutorial.keys)
            {
                if (Input.GetKeyDown(key) && !pressed)
                {
                    pressed = true;
                    StartCoroutine(Hide(3));
                }
            }
        }
        if (triggered && !played)
        {
            if (FindObjectOfType<TutorialManager>().showing && !tutorial.advanced) return;
            played = true;
            StartCoroutine(Show(delay));
            StartCoroutine(Hide(15));
        }
    }

    IEnumerator Show(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        FindObjectOfType<TutorialManager>().ShowTutorial(tutorial);
    }

    IEnumerator Hide(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        FindObjectOfType<TutorialManager>().HideTutorial();
        Destroy(gameObject);
    }
}
