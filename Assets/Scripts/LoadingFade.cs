using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingFade : MonoBehaviour
{
    public Image background;
    public float fadeSpeed = 0.01f;
    public bool fadingIn;
    // Start is called before the first frame update


    private void Update()
    {
        float alpha = background.color.a;

        if (fadingIn)
            alpha = Mathf.Clamp(alpha + Time.deltaTime * fadeSpeed, 0, 1);
        else
            alpha = Mathf.Clamp(alpha - Time.deltaTime * fadeSpeed, 0, 1);

        background.color = new Color(0, 0, 0, alpha);

        if (background.color.a == 0)
        {
            GetComponent<Canvas>().sortingOrder = -100;
        } else if (background.color.a >= 0.01f)
        {
            GetComponent<Canvas>().sortingOrder = 100;
        }
    }
}
