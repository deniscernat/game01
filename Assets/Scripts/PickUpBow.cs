using UnityEngine;

public class PickUpBow : MonoBehaviour
{
    private void Update()
    {
        if (ScoreManager.Instance.bowPickedUp)
            Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            try
            {
                ScoreManager.Instance.bowPickedUp = true;
                FindObjectOfType<AudioManager>().Play("pick_up");
                Destroy(gameObject);
            }
            catch { }
        }
    }
}
