﻿using UnityEngine.UI;
using TMPro;
using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class JumpCharge : MonoBehaviour
{
    public Slider slider;
    //public RectTransform maxFill;
    public List<RectTransform> maxFills = new List<RectTransform>();
    private RectTransform parent;

    private void Awake()
    {
        parent = GetComponent<RectTransform>();
    }

    private void Update()
    {
        parent = GetComponent<RectTransform>();
        try
        {
            float markerPosX = parent.sizeDelta.x * FindObjectOfType<Tanya>().jumpChargeToStun;
            float markerWidthX = parent.sizeDelta.x - parent.sizeDelta.x * FindObjectOfType<Tanya>().jumpChargeToStun;
            foreach (RectTransform maxFill in maxFills)
            {
                maxFill.localPosition = new Vector3(markerPosX, maxFill.localPosition.y, maxFill.localPosition.z);
                maxFill.sizeDelta = new Vector2(markerWidthX, maxFill.sizeDelta.y);
            }
        } catch { }
    }

    public void SetMaxHeight(float value)
    {
        slider.maxValue = value;
        slider.value = value;

        string s = System.Math.Round(slider.value, 2).ToString();
    }

    public void SetHeight(float value)
    {
        slider.value = value;
        string s = System.Math.Round(slider.value, 2).ToString();
    }
}
