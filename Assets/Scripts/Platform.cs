﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform pos1, pos2;
    public float speed;
    public Transform startPos;
    private Transform targetParent;
    Vector3 nextPos;

    void Start()
    {
        nextPos = startPos.position;
    }
    void Update()
    {
        if (transform.position == pos1.position)
            nextPos = pos2.position;
        else if (transform.position == pos2.position)
            nextPos = pos1.position;

        transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(pos1.position, pos2.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            targetParent = collision.collider.transform.parent;
            collision.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            targetParent = null;
            collision.collider.transform.SetParent(targetParent);
        }
    }
}
