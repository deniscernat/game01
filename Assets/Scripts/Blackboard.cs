using UnityEngine;
using System.Collections;

public class Blackboard : MonoBehaviour
{
    public float playerDetectionRadius;
    public static bool drawing;
    Transform player;
    public Canvas UI;
    public Canvas buttons;
    public bool interactable = true;
    public Vector3 positionOffset;
    public float zoom;
    public GameObject drawingCanvas;
    public Drawing newCanvas;

    private void Start()
    {
        drawing = false;
        try
        {
            player = FindObjectOfType<PlayerMovementController>().transform;
        }
        catch { }
    }

    private void Update()
    {
        if (FindObjectOfType<PlayerMovementController>() & !FindObjectOfType<CharacterSwitch>().dead)
            player = FindObjectOfType<PlayerMovementController>().transform;


        if (interactable)
        {
            Vector2 distanceToPlayer = player.position - (transform.position + positionOffset);

            if (distanceToPlayer.magnitude < playerDetectionRadius && !FindObjectOfType<DialogueManager>().playing)
            {
                if (!drawing)
                    UI.gameObject.SetActive(true);
            } else
                UI.gameObject.SetActive(false);

            if (Input.GetKeyDown(KeyCode.F))
            {
                if (distanceToPlayer.magnitude < playerDetectionRadius && !FindObjectOfType<DialogueManager>().playing)
                {
                    if (!drawing)
                    {
                        Enter();
                        return;
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) && !FindObjectOfType<TutorialManager>().advancedActive)
            {
                if (drawing)
                    Exit();
            }

            if (drawing && newCanvas != null)
            {
                newCanvas.transform.position = Vector3.zero;
                FindObjectOfType<CameraControl>().transform.position = transform.position;
            }
        }
    }

    public void Undo()
    {
        try
        {
            if (newCanvas.brushes.Count > 1)
            {
                newCanvas.Undo();
                newCanvas.Undo();
            }
        }
        catch { }
    }

    public void Clear()
    {
        Destroy(newCanvas.gameObject);
        newCanvas = Instantiate(drawingCanvas).GetComponent<Drawing>();
        newCanvas.width = GetComponent<BoxCollider2D>().size.x;
        newCanvas.height = GetComponent<BoxCollider2D>().size.y;
    }

    public void Enter()
    {
        drawing = true;
        ScoreManager.Instance.lockCharacter = true;
        FindObjectOfType<CameraControl>().drawing = true;
        FindObjectOfType<CameraControl>().transform.position = transform.position;
        FindObjectOfType<Zoom>().ZoomTo(zoom);
        UI.gameObject.SetActive(false);
        buttons.gameObject.SetActive(true);

        if (newCanvas == null)
            newCanvas = Instantiate(drawingCanvas).GetComponent<Drawing>();
        newCanvas.allowDrawing = true;
        newCanvas.width = GetComponent<BoxCollider2D>().size.x;
        newCanvas.height = GetComponent<BoxCollider2D>().size.y;
    }
    public void Exit()
    {
        PauseMenu.allowPause = false;
        ScoreManager.Instance.lockCharacter = false;
        newCanvas.allowDrawing = false;
        UI.gameObject.SetActive(true);
        buttons.gameObject.SetActive(false);
        drawing = false;
        FindObjectOfType<CameraControl>().transform.SetParent(null);
        FindObjectOfType<CameraControl>().drawing = false;
        FindObjectOfType<Zoom>().ZoomOut();
        StartCoroutine(EnablePause());
    }

    IEnumerator EnablePause()
    {
        yield return new WaitForEndOfFrame();
        PauseMenu.allowPause = true;
    }
}
