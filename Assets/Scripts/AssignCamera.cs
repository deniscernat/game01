﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignCamera : MonoBehaviour
{
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
    }

    private void Update()
    {
        if (GetComponent<Canvas>().worldCamera == null)
            GetComponent<Canvas>().worldCamera = Camera.main;
    }
}
