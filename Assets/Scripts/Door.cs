using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public float playerDetectionRadius;
    public bool open;
    Transform player;
    public Transform doorMain;
    public Canvas doorUI;
    public string levelName;
    public bool interactable = true;

    private void Start()
    {
        try
        {
            player = FindObjectOfType<PlayerMovementController>().transform;
        } catch { }
    }

    private void Update()

    {
        if (FindObjectOfType<PlayerMovementController>() & !FindObjectOfType<CharacterSwitch>().dead)
        {
            player = FindObjectOfType<PlayerMovementController>().transform;
        } else
        {
            return;
        }

        if (interactable)
        {
            Vector2 distanceToPlayer = player.position - transform.position;
            if (distanceToPlayer.magnitude < playerDetectionRadius && !FindObjectOfType<DialogueManager>().playing && !PhoneManager.Instance.calling)
            {
                doorUI.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (!open)
                    {
                        FindObjectOfType<AudioManager>().Play("door_open");
                        doorMain.rotation = new Quaternion(0, 180, 0, 0);
                        open = true;
                    }
                    else
                    {
                        FindObjectOfType<AudioManager>().Play("door_close");
                        player.parent.gameObject.SetActive(false);
                        doorMain.rotation = Quaternion.identity;
                        open = false;
                        SaveData.Save(levelName) ;
                        StartCoroutine(LoadLevel());
                    }

                }
            }
            else
            {
                doorUI.gameObject.SetActive(false);
                if (open)
                {
                    FindObjectOfType<AudioManager>().Play("door_close");
                    doorMain.rotation = Quaternion.identity;
                    open = false;
                }

            }
        }
    }

    IEnumerator LoadLevel()
    {
        FindObjectOfType<UIManager>().levelTransition.SetTrigger("Start");

        yield return new WaitForSeconds(1);

        FindObjectOfType<UIManager>().levelTransition.SetTrigger("End");

        Loader.Load(levelName);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, playerDetectionRadius);
    }
}
