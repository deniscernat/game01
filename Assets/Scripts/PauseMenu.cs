using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;
    public static bool allowPause = true;
    public GameObject pauseMenuUI;

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().name != "Main Menu" && allowPause)
        {
            if (gameIsPaused)
            {
                if (FindObjectOfType<TutorialManager>().advancedActive)
                    FindObjectOfType<TutorialManager>().HideTutorial();
                else Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        if (ScoreManager.Instance.isDead)
        {
            FindObjectOfType<UIManager>().deathScreen.gameObject.SetActive(true);
            FindObjectOfType<ShowDeathScreen>().show();
        }
        else
        {
            FindObjectOfType<UIManager>().depthOfField.focusDistance.value = 3.5f;

        }
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Pause()
    {
        if (ScoreManager.Instance.isDead)
        {
            FindObjectOfType<ShowDeathScreen>().hide();
            FindObjectOfType<UIManager>().deathScreen.gameObject.SetActive(false);
        }
        else
        {
            FindObjectOfType<UIManager>().depthOfField.focusDistance.value = 0.9f;
        }
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void LoadMenu()
    {
        Loader.Load("Main Menu");
        PhoneManager.Instance.calling = false;
        PhoneManager.Instance.phoneUp = false;
        PhoneManager.Instance.phoneWasCalled = false;
        DialogueManager.instance.Stop();
        TutorialManager.instance.HideTutorial();
        Resume();
    }

}
