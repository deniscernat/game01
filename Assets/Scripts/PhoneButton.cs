using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PhoneButton : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!ScoreManager.Instance.isDead)
            PhoneManager.Instance.AcceptCall();
    }
}
