using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaySwaps : MonoBehaviour
{
    public List<GameObject> swaps = new List<GameObject>();
    void Update()
    {
        if (ScoreManager.Instance.SerejaUnlocked)
            swaps[0].SetActive(true);
        else swaps[0].SetActive(false);

        if (ScoreManager.Instance.RomaUnlocked)
            swaps[1].SetActive(true);
        else swaps[1].SetActive(false);

        if (ScoreManager.Instance.TanyaUnlocked)
            swaps[2].SetActive(true);
        else swaps[2].SetActive(false);

        int unlockedCharacters = 0;
        foreach(GameObject swap in swaps)
        {
            if (swap.activeSelf)
                unlockedCharacters++;
        }
        if (unlockedCharacters < 2)
        {
            foreach (GameObject swap in swaps)
            {
                swap.SetActive(false);
            }
        } else return;
            
    }
}
