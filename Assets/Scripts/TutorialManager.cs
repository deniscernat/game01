using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public static TutorialManager instance;
    public TMP_Text title;
    public TMP_Text text;
    public TMP_Text advTitle;
    public TMP_Text advText;
    public Image image;
    public List<Image> advImages = new List<Image>();
    public Color tint;
    public bool showing;

    public bool advancedActive;
    bool colorChanged = false;
    Volume volume;
    ColorAdjustments colorAdjustments;
    DepthOfField depthOfField;
    Color defaultTint;

    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        volume = FindObjectOfType<Volume>();
        volume.profile.TryGet<DepthOfField>(out depthOfField);
        volume.profile.TryGet<ColorAdjustments>(out colorAdjustments);
        defaultTint = colorAdjustments.colorFilter.value;
    }

    private void Update()
    {
        volume = FindObjectOfType<Volume>();
        volume.profile.TryGet<DepthOfField>(out depthOfField);
        volume.profile.TryGet<ColorAdjustments>(out colorAdjustments);
    }

    public void ShowTutorial(Tutorial tutorial)
    {
        if (tutorial.advanced)
        {
            advTitle.transform.parent.gameObject.SetActive(true);
            advTitle.text = tutorial.title;
            advText.text = tutorial.text;
            for (int i = 0; i < advImages.Count; i++)
            {
                advImages[i].sprite = tutorial.images[i];
            }
            advTitle.GetComponentInParent<CanvasGroup>().alpha = 1;
            advTitle.GetComponentInParent<CanvasGroup>().interactable = true;
            PauseMenu.gameIsPaused = true;
            depthOfField.focusDistance.value = 0.9f;
            advancedActive = true;
            colorAdjustments.colorFilter.Override(tint);
            Time.timeScale = 0;
        }
        else
        {
            title.text = tutorial.title;
            text.text = tutorial.text;
            image.sprite = tutorial.images[0];
            showing = true;
            FindObjectOfType<AudioManager>().Play("short_swoop");
            title.GetComponentInParent<Animator>().SetBool("Showed", true);
        }
    }

    public void HideTutorial()
    {
        if (advancedActive)
        {
            colorAdjustments.colorFilter.Override(defaultTint);
            Time.timeScale = 1;
            PauseMenu.gameIsPaused = false;
            depthOfField.focusDistance.value = 3.5f;
            advTitle.GetComponentInParent<CanvasGroup>().alpha = 0;
            advTitle.GetComponentInParent<CanvasGroup>().interactable = false;
            advancedActive = false;
            advTitle.transform.parent.gameObject.SetActive(false);
            return;
        }
        showing = false;
        FindObjectOfType<AudioManager>().Play("swoop");
        title.GetComponentInParent<Animator>().SetBool("Showed", false);
    }
}
