using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SaveData
{
    public static void Save(string levelName)
    {
        ES2.Save(levelName, "savedScene");

        ES2.Save(ScoreManager.Instance.coins, "coins");
        ES2.Save(ScoreManager.Instance.currentHealth, "currentHealth");
        ES2.Save(ScoreManager.Instance.grades, "grades");
        ES2.Save(ScoreManager.Instance.activeCharacterIndex, "activeCharacterIndex");
        ES2.Save(ScoreManager.Instance.backpackPickedUp, "backpackPickedUp");
        ES2.Save(ScoreManager.Instance.bowPickedUp, "bowPickedUp");
    }
}
