using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneManager : AutoCleanupSingleton<PhoneManager>
{
    public bool phoneWasCalled;
    public bool phoneUp = false;
    public bool calling = false;

    [SerializeField]public void AcceptCall()
    {
        calling = false;
        FindObjectOfType<AudioManager>().Play("beep");
    }
}
