using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager instance;

    public TMP_Text nameText;
    public TMP_Text dialogueText;
    public float textSpeed;
    public bool playing;

    public List<Dialogue> dialogues = new List<Dialogue>();
    Queue<string> names = new Queue<string>();
    Queue<string> sentences = new Queue<string>();
    Queue<Sound> voiceLines = new Queue<Sound>();

    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        nameText = FindObjectOfType<UIManager>().dialogueName;
        dialogueText = FindObjectOfType<UIManager>().dialogueText;
        if (dialogues.Count > 0 && !playing && !PhoneManager.Instance.calling)
            StartDialogue(dialogues[0]);

        if (PauseMenu.gameIsPaused)
        {
            if (dialogues.Count > 0)
            {
                foreach (VoiceLine line in dialogues[0].lines)
                {
                    if (line.audio.source != null)
                        line.audio.source.pitch = 0;
                }
            }
            
        }
        if (!PauseMenu.gameIsPaused)
        {
            if (dialogues.Count > 0) 
            {
                foreach (VoiceLine line in dialogues[0].lines)
                {
                    if (line.audio.source != null)
                        line.audio.source.pitch = 1;
                }
            }
            
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        if (dialogue.phoneCall)
        {
            FindObjectOfType<AudioManager>().Play("beep");
            PhoneManager.Instance.phoneUp = true;
        }

        Color color = new Color(0, 0, 0, .4f);
        nameText.transform.GetComponentInParent<Image>().color = color;

        sentences.Clear();
        voiceLines.Clear();

        foreach (VoiceLine line in dialogue.lines)
        {
            if (line.audio != null)
            {
                line.audio.source = gameObject.AddComponent<AudioSource>();
                line.audio.source.clip = line.audio.clip;

                line.audio.source.volume = line.audio.volume;
                line.audio.source.pitch = line.audio.pitch;
                line.audio.source.loop = line.audio.loop;
            }
            names.Enqueue(line.name);
            sentences.Enqueue(line.sentence);
            voiceLines.Enqueue(line.audio);
        }
        playing = true;
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            Invoke("EndDialogue", 2);
            return;
        }

        string name = names.Dequeue();
        string sentence = sentences.Dequeue();
        Sound voiceLine = voiceLines.Dequeue();
        voiceLine.source.Play();
        StartCoroutine(TypeSentence(sentence));
        nameText.text = name;
    }

    IEnumerator TypeSentence(string sentece)
    {
        dialogueText.text = "";
        foreach (char letter in sentece.ToCharArray())
        {
            dialogueText.text += letter;
            if (dialogueText.text == sentece)
            {
                Invoke("DisplayNextSentence", 1);
            }
            yield return new WaitForSeconds(textSpeed);
        }
    }

    public void EndDialogue()
    {
        dialogues.RemoveAt(0);
        playing = false;
        Color color = new Color(0, 0, 0, 0);
        nameText.transform.GetComponentInParent<Image>().color = color;
        nameText.text = "";
        dialogueText.text = "";
        PhoneManager.Instance.phoneUp = false;
    }

    public void Stop()
    {
        StopAllCoroutines();
        Color color = new Color(0, 0, 0, 0);
        nameText.transform.GetComponentInParent<Image>().color = color;
        nameText.text = "";
        dialogueText.text = "";
        playing = false;
        try
        {
            if (dialogues.Count > 0)
            {
                foreach (VoiceLine line in dialogues[0].lines)
                {
                    if (line.audio.source != null)
                        Destroy(line.audio.source);
                }
            }
        }
        catch { }
        dialogues.Clear();
    }
}
