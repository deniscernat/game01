using UnityEngine;

public class TrapDamage : MonoBehaviour
{
    public Trap_drop parent;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision && collision.tag == "Player")
        {
            parent.goingDown = false;
            if (parent.instant)
                collision.GetComponent<Player>().Kill();
            else
            {
                collision.GetComponent<Player>().TakeDamage(parent.damage);
                DamageIndicator.Create(collision.transform.position, -parent.damage);
            }
        }
    }
}
