﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play("Coin");
            ScoreManager.Instance.coins++;

            Destroy(gameObject);
        }
    }
}
