using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteAlways]
public class FloorSystem : MonoBehaviour
{
    public int currentFloor = 0;
    public List<Transform> floorMarkers = new List<Transform>();
    public List<GameObject> floors = new List<GameObject>();
    float playerY;

    private void Start()
    {
        try
        {
            playerY = FindObjectOfType<Player>().transform.position.y;
        }
        catch { }
    }

    private void LateUpdate()
    {
        try
        {
             playerY = FindObjectOfType<Player>().transform.position.y;
        } catch { }
        for (int i = 0; i < floorMarkers.Count - 1; i++)
        {
            if (playerY > floorMarkers[i].position.y & playerY < floorMarkers[i + 1].position.y)
            {
                currentFloor = i;
                break;
            }
            currentFloor = floorMarkers.Count - 1;
        }
        for (int i = 0; i < floors.Count; i++)
        {
            if (i == currentFloor)
            {
                floors[i].GetComponent<Animator>().SetBool("visible", false);

            }
            else
            {
                floors[i].GetComponent<Animator>().SetBool("visible", true);
            }
        }
    }
}

