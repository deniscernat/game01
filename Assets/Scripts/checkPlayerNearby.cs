﻿using UnityEngine;

public class CheckPlayerNearby : MonoBehaviour
{
    public LayerMask playerLayer;
    public Animator animator;
    [Range(1f, 100f)]
    public float detectionRadius = 12f;

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Physics2D.OverlapCircle(transform.position, detectionRadius, playerLayer))
        {
            animator.SetBool("open", true);
            GetComponent<Floater>().activated = true;
        } else
        {
            GetComponent<Floater>().activated = false;
            animator.SetBool("open", false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}
