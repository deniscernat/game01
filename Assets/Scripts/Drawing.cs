using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    public Camera m_camera;
    public GameObject brush;
    public List<GameObject> brushes = new List<GameObject>();
    public bool allowDrawing = true;
    public float width, height;
    public Vector3 center;

    LineRenderer currentLineRenderer;

    Vector2 lastPos;

    private void Awake()
    {
        m_camera = Camera.main;
    }

    private void Update()
    {
        center = FindObjectOfType<CameraControl>().transform.position;

        if (allowDrawing)
            Draw();
        else return;

    }

    void Draw()
    {
        Vector2 mousePos = m_camera.ScreenToWorldPoint(Input.mousePosition);
        if (mousePos.y <= center.y + height / 2 &&
                    mousePos.y >= center.y - height / 2 &&
                    mousePos.x <= center.x + width / 2 &&
                    mousePos.x >= center.x - width / 2)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                CreateBrush();
                int sfxIndex = Random.Range(1, 6);
                string sfxName = string.Concat("chalk", sfxIndex.ToString());
                FindObjectOfType<AudioManager>().Play(sfxName);

            }
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (mousePos != lastPos)
                {

                    AddPoint(mousePos);
                    lastPos = mousePos;
                }
            }
            else
            {
                currentLineRenderer = null;
            }
        }
    }

    void CreateBrush()
    {
        GameObject brushInstance = Instantiate(brush, transform);
        brushes.Add(brushInstance);
        currentLineRenderer = brushInstance.GetComponent<LineRenderer>();

        Vector2 mousePos = m_camera.ScreenToWorldPoint(Input.mousePosition);

        currentLineRenderer.SetPosition(0, mousePos);
        currentLineRenderer.SetPosition(1, mousePos);
    }

    public void Undo()
    {
        Destroy(brushes[brushes.Count - 1]);
        brushes.RemoveAt(brushes.Count - 1);
        currentLineRenderer = brushes[brushes.Count - 1].GetComponent<LineRenderer>();
    }

    void AddPoint(Vector2 pointPos)
    {
        if (currentLineRenderer)
        {
            currentLineRenderer.positionCount++;
            int positionIndex = currentLineRenderer.positionCount - 1;
            currentLineRenderer.SetPosition(positionIndex, pointPos);
        }
        
    }
}
