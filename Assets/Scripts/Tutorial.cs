using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tutorial
{
    public string title;
    [TextArea(3, 10)]
    public string text;
    public List<Sprite> images = new List<Sprite>();
    public KeyCode[] keys;
    public bool advanced;
}