﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSwitch : MonoBehaviour
{
    public static GameObject activeCharacter;
    private Vector3 currentPosition;
    public List<GameObject> characters = new List<GameObject>();
    public List<GameObject> abilities = new List<GameObject>();
    public List<GameObject> avatars = new List<GameObject>();
    public ParticleSystem effect;
    public List<GameObject> avatarsSmall = new List<GameObject>();
    public float timeToSwitch;
    [SerializeField]
    private float switchDelay;
    public bool dead;
    public GameObject fuelBar;
    public GameObject jumpBar;

    void Awake()
    {
        foreach (GameObject character in characters)
        {
            if (character.transform.parent.gameObject.activeSelf)
            {
                activeCharacter = character;
                ScoreManager.Instance.activeCharacterIndex = characters.IndexOf(character);
            } else
            {
                activeCharacter = characters[ScoreManager.Instance.activeCharacterIndex];
            }
        }
        activeCharacter.transform.parent.gameObject.SetActive(true);
        activeCharacter.transform.position = FindObjectOfType<PlayerRespawn>().transform.position;
        currentPosition = activeCharacter.transform.position;

        for (int i = 0; i < avatars.Count; i++)
        {
            Image image = avatarsSmall[i].GetComponent<Image>();

            if (i == ScoreManager.Instance.activeCharacterIndex)
            {
                var color = image.color;
                color.a = 1f;
                image.color = color;
                avatars[i].SetActive(true);
            }
            else
            {
                avatars[i].SetActive(false);
                var color = image.color;
                color.a = 0.3f;
                image.color = color;
            }
        }
    }
    void Update()
    {
        if (PauseMenu.gameIsPaused) return;

        if (!dead & !FindObjectOfType<AudioManager>().GetComponents<AudioSource>()[36].isPlaying)
            FindObjectOfType<AudioManager>().Play("soundtrack_1");

        if (Time.time >= timeToSwitch && !dead && !ScoreManager.Instance.lockCharacter)
        {
            if (Input.GetKeyDown("1"))
            {
                if (ScoreManager.Instance.activeCharacterIndex != 0)
                    ScoreManager.Instance.activeCharacterIndex = 0;
            }

            if (Input.GetKeyDown("2") && ScoreManager.Instance.RomaUnlocked)
            {
                if (ScoreManager.Instance.activeCharacterIndex != 1)
                    ScoreManager.Instance.activeCharacterIndex = 1;
            }

            if (Input.GetKeyDown("3") && ScoreManager.Instance.TanyaUnlocked)
            {
                if (ScoreManager.Instance.activeCharacterIndex != 2)
                    ScoreManager.Instance.activeCharacterIndex = 2;
            }
        }

        if (ScoreManager.Instance.activeCharacterIndex == 0 && ScoreManager.Instance.backpackPickedUp)
            fuelBar.SetActive(true);
        else
            fuelBar.SetActive(false);

        if (ScoreManager.Instance.activeCharacterIndex == 2)
            jumpBar.SetActive(true);
        else
            jumpBar.SetActive(false);

        if (ScoreManager.Instance.activeCharacterIndex != characters.IndexOf(activeCharacter))
            SwitchCharacter(ScoreManager.Instance.activeCharacterIndex);

        activeCharacter = characters[ScoreManager.Instance.activeCharacterIndex];
        FindObjectOfType<PlayerRespawn>().currentPlayer = gameObject;
        effect.transform.position = currentPosition;
        currentPosition = activeCharacter.transform.position;
    }

    void SwitchCharacter(int index)
    {
        if (index == 0)
        {
            SwapCharacter(0);
            Shooting ability = FindObjectOfType<Shooting>();
            if (ability.sonic)
                ability.ResumeSonic();
            else
                ability.ResumeCooldown();

        }
        else if (index == 1)
        {
            SwapCharacter(1);
            Genji ability = FindObjectOfType<Genji>();
            ability.ResumeCooldown();
        }
        else if (index == 2)
        {
            SwapCharacter(2);
            Tanya tanya = FindObjectOfType<Tanya>();
            tanya.chargingJump = false;
            LaunchHook ability1 = FindObjectOfType<LaunchHook>();
            ability1.ResumeCooldown();
            ActivateShield ability2 = FindObjectOfType<ActivateShield>();
            ability2.ResumeCooldown();
        }
    }

    void ActivateCharacter()
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if (characters[i].name != activeCharacter.name)
            {
                characters[i].GetComponent<Player>().parent.SetActive(false);
                characters[i].SetActive(false);
            }
            else
            {
                characters[i].GetComponent<Player>().parent.SetActive(true);
                characters[i].SetActive(true);
            }
        }

        for (int i = 0; i < avatars.Count; i++)
        {
            if (i == ScoreManager.Instance.activeCharacterIndex)
            {
                avatars[i].SetActive(true);
                Image image = avatarsSmall[i].GetComponent<Image>();
                var color = image.color;
                color.a = 1f;
                image.color = color;
            }
            else
            {
                avatars[i].SetActive(false);
                Image image = avatarsSmall[i].GetComponent<Image>();
                var color = image.color;
                color.a = 0.3f;
                image.color = color;
            }
        }
    }

    void SwapCharacter(int index)
    {
        FindObjectOfType<AudioManager>().Play("Swap");
        FindObjectOfType<AudioManager>().GetComponents<AudioSource>()[13].enabled = false;
        ScoreManager.Instance.activeCharacterIndex = index;
        var oldCharacter = activeCharacter;
        activeCharacter = characters[ScoreManager.Instance.activeCharacterIndex];
        activeCharacter.transform.position = oldCharacter.transform.position;
        for (int i = 0; i < characters.Count; i++)
        {
            characters[i].GetComponent<SpriteRenderer>().color = Color.white;
            characters[i].transform.parent.GetChild(1).GetComponent<SpriteRenderer>().color = Color.white;
            characters[i].GetComponent<PlayerMovementController>().jumpMultiplier = 0;
        }
        characters[2].GetComponent<Tanya>().landed = true;
        effect.Play();
        ActivateCharacter();
        timeToSwitch = Time.time + switchDelay;
    }
}