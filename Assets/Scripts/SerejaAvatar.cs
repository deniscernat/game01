using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SerejaAvatar : MonoBehaviour
{
    public Sprite backpack_on;
    public Sprite backpack_off;
    // Update is called once per frame
    void Update()
    {
        if (ScoreManager.Instance.backpackPickedUp)
            GetComponent<Image>().sprite = backpack_on;
        else
            GetComponent<Image>().sprite = backpack_off;
    }
}
