using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : AutoCleanupSingleton<ScoreManager>
{
    [SerializeField] public int coins;
    [SerializeField] public float maxHealth = 10f;
    [SerializeField] public float minHealth = 5f;
    [SerializeField] public float currentHealth;
    [SerializeField] public bool isDead;
    [SerializeField] public List<int> grades = new List<int>();
    [Range(0, 2)]
    [SerializeField] public int activeCharacterIndex = 0;
    [SerializeField] public bool backpackPickedUp;
    [SerializeField] public bool bowPickedUp;
    [SerializeField] public bool lockCharacter;
    [SerializeField] public bool SerejaUnlocked = true;
    [SerializeField] public bool RomaUnlocked = true;
    [SerializeField] public bool TanyaUnlocked = true;
}
