﻿using UnityEngine;
using EZCameraShake;

public class Tanya : MonoBehaviour
{
    public PlayerMovementController playerController;
    Player player;
    [HideInInspector]
    public bool landed;
    public bool chargingJump;
    public float jumpCharge = 0;
    [Range(0, 1)]
    public float jumpChargeToStun = 0.8f;
    public float jumpChargeRate;
    public float stunRadius;
    public float stunDamage;
    public float stunHeight;
    public ParticleSystem landingSmoke;
    public ParticleSystem dustPS;

    public JumpCharge jumpChargeController;

    void Start()
    {
        playerController = GetComponent<PlayerMovementController>();
        player = GetComponent<Player>();
    }

    void Update()
    {
        landingSmoke.transform.position = playerController.groundCheck.position + new Vector3(0, 1.4f, 0);

        if (Input.GetKeyDown(KeyCode.LeftShift))
            playerController.GetComponent<Rigidbody2D>().velocity = new Vector2(0, playerController.GetComponent<Rigidbody2D>().velocity.y);

        if (playerController.grounded)
        {
            if (!landed)
            {
                if (jumpCharge > jumpChargeToStun)
                {
                    Collider2D[] enemies = Physics2D.OverlapAreaAll(new Vector2(transform.position.x - stunRadius, transform.position.y), new Vector2(transform.position.x + stunRadius, transform.position.y - transform.localScale.y / 2), playerController.enemyMask);
                    foreach (Collider2D enemy in enemies)
                    {
                        enemy.GetComponent<Rigidbody2D>().AddForce(Vector2.up * stunHeight);
                        enemy.GetComponent<EnemyHP>().TakeDamage(stunDamage);
                        DamageIndicator.Create(enemy.transform.position, stunDamage);
                    }
                    landingSmoke.Play();
                    FindObjectOfType<AudioManager>().Play("FallHard");
                    try
                    {
                        CameraShaker.Instance.ShakeOnce(player.magnitude * 3, player.roughness, player.fadein, player.fadeout);
                    } catch { }
                }
                jumpCharge = 0;
                landed = true;
            }
        } else landed = false;

        if(Input.GetButtonDown("Jump") && !ScoreManager.Instance.lockCharacter)
            chargingJump = true;

        if (Input.GetButtonUp("Jump") && !ScoreManager.Instance.lockCharacter)
        {
            dustPS.Play();
            chargingJump = false;
            playerController.zoomMagnifier = 0;
        }

        playerController.fallMultilier = jumpCharge > jumpChargeToStun ? 12f : 5.5f;

        jumpChargeController.SetHeight(jumpCharge);

        if(PauseMenu.gameIsPaused)
        {
            chargingJump = false;
            playerController.zoomMagnifier = 0;
            jumpCharge = 0;
        }
    }

    private void FixedUpdate()
    {
        if (chargingJump)
        {
            jumpCharge = Mathf.Clamp(jumpCharge + jumpChargeRate * Time.deltaTime, 0, 1);
            if (jumpCharge < 1)
                playerController.zoomMagnifier = Mathf.Clamp(playerController.zoomMagnifier + 3.4f * Time.deltaTime, 0, 3f);
        }
        if (jumpCharge == 1 && playerController.grounded)
        {
            try
            {
                CameraShaker.Instance.ShakeOnce(player.magnitude / 5, player.roughness, player.fadein, player.fadeout);
            }
            catch { }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(new Vector2(transform.position.x - stunRadius, transform.position.y), 0.2f);
        Gizmos.DrawWireSphere(new Vector2(transform.position.x + stunRadius, transform.position.y - transform.localScale.y / 2), 0.2f);
    }
}


