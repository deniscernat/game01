using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Button continueButton;
    public static bool loading;
    public bool continueButtonEnabled = true;

    private void Start()
    {
        FindObjectOfType<AudioManager>().StopAll();
    }
    private void Update()
    {
        try
        {
            FindObjectOfType<ShowDeathScreen>().hide();
        }
        catch { }
        ScoreManager.Instance.isDead = false;


        foreach (Button button in transform.GetChild(0).GetComponentsInChildren<Button>())
        {
            if (loading)
                button.interactable = false;
        }
        try
        {
            var scene = ES2.Load<string>("savedScene");
            continueButtonEnabled = true;
        }
        catch
        {
            continueButtonEnabled = false;
        }

        if (continueButtonEnabled)
        {
            continueButton.enabled = true;
            continueButton.interactable = true;
        }
        else
        {
            continueButton.enabled = false;
            continueButton.interactable = false;
        }
            

    }

    public void NewGame()
    {
        FindObjectOfType<UIManager>().levelTransition.SetTrigger("Start");
        loading = true;
        void func()
        {
            LoadData.NewGame();
        }
        StartCoroutine(FinishFirst(FindObjectOfType<UIManager>().fadeAnimation.length, func));
    }

    public void Continue()
    {
        FindObjectOfType<UIManager>().levelTransition.SetTrigger("Start");
        void func()
        {
            LoadData.Load();
        }
        StartCoroutine(FinishFirst(FindObjectOfType<UIManager>().fadeAnimation.length, func));
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator FinishFirst(float seconds, Action doLast)
    {
        yield return new WaitForSeconds(seconds);
        doLast();
        loading = false;
        Player.loaded = false;
    }
}
