using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayBow : MonoBehaviour
{
    void Update()
    {
        Color color;
        if (ScoreManager.Instance.bowPickedUp)
        {
            color = new Color(1, 1, 1, 1);
        } else
        {
            color = new Color(1, 1, 1, 0);
        }
        GetComponent<SpriteRenderer>().color = color;

    }
}
