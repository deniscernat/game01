using UnityEngine;

public class PickUpBackup : MonoBehaviour
{
    private void Update()
    {
        if (ScoreManager.Instance.backpackPickedUp)
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
   {
        if (other.tag == "Player")
        {
            try
            {
                ScoreManager.Instance.backpackPickedUp = true;
                FindObjectOfType<AudioManager>().Play("pick_up");
                Destroy(gameObject);
            }
            catch { }
        }
   }
}
