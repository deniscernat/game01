using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Trap_drop : MonoBehaviour
{
    public List<SpriteRenderer> cables= new List<SpriteRenderer>();
    public List<ParticleSystem> particles = new List<ParticleSystem>();
    public Transform body;
    public Transform target;

    public float delay = 0f;
    public float interval = 1f;
    public float speed;
    public bool instant;
    [Range(1, 10)]
    public int damage;

    public bool goingDown = false;

    float minInterval;
    float timeToDrop = 0f;
    float cableLength = 0f;
    bool active = false;

    private void Start()
    {
        timeToDrop = delay;
    }

    private void Update()
    {
        // Cables
        cableLength = Vector2.Distance(transform.position, body.position);
        foreach (SpriteRenderer cable in cables)
        {
            cable.size = new Vector2(cables[0].size.x, cableLength);
        }

        // Periodic activation
        if (Time.time >= timeToDrop)
            Drop();

        // Calculate minimum interval
        float distance = Mathf.Abs(target.position.y - transform.position.y);
        minInterval = (distance / speed) * 1.5f;
        if (interval < minInterval)
            interval = minInterval;
    }

    private void FixedUpdate()
    {
        if (active && goingDown)
        {
            body.position = Vector3.MoveTowards(body.position, target.position, speed * 2 * Time.deltaTime);

            if (target.position.y < transform.position.y)
            {
                if (body.position.y <= target.position.y)
                    goingDown = false;
            } else
            {
                if (body.position.y >= target.position.y)
                    goingDown = false;
            }
        } 
        if (active && !goingDown)
        {
            body.position = Vector3.MoveTowards(body.position, transform.position, speed * Time.deltaTime);

            if (target.position.y < transform.position.y)
            {
                if (body.position.y >= transform.position.y)
                    active = false;
            }
            else
            {
                if (body.position.y <= transform.position.y)
                    active = false;
            }
        }
    }

    public void Drop()
    {
        goingDown = true;
        active = true;
        foreach (ParticleSystem smoke in particles)
        {
            smoke.Play();
        }
        timeToDrop = Time.time + interval;
    }

}
