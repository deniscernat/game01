using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Stairs : MonoBehaviour
{
    public bool isFront = true;
    public static bool collisinEnabled;
    public bool alwaysSolid = false;
    public Sprite frontSprite;
    public Sprite backSprite;

    void Update()
    {
        if (isFront)
        {
            GetComponent<SpriteRenderer>().sprite = frontSprite;
            GetComponent<SpriteRenderer>().sortingLayerName = "stairs_front";
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = backSprite;
            GetComponent<SpriteRenderer>().sortingLayerName = "stairs_back";
            transform.rotation = new Quaternion(0, 180, 0, 0);
        }

        if (!collisinEnabled & !alwaysSolid)
        {
            GetComponent<EdgeCollider2D>().enabled = false;

        }
        else
        {
            GetComponent<EdgeCollider2D>().enabled = true;
        }
    }
}
