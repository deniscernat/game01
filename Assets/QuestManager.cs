using System.Collections;
using TMPro;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    public TMP_Text questText;
    public bool showing;
    public Animator animator;
    public static QuestTrigger activeQuest;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            if (showing)
                Hide(1f);
            else
                Show("test", 0f);
        }
    }

    public void Show(string text, float delay)
    {
        StartCoroutine(ShowQuestAfter(text, delay));
    }

    IEnumerator ShowQuestAfter(string text, float delay)
    {
        yield return new WaitForSeconds(delay);
        questText.fontStyle = FontStyles.Normal;
        questText.text = text;
        animator.SetBool("showing", true);
        showing = true;
    }

    public void Hide(float delay)
    {
        questText.fontStyle = FontStyles.Strikethrough;
        StartCoroutine(HideQuestAfter(delay));
    }

    IEnumerator HideQuestAfter(float delay)
    {
        yield return new WaitForSeconds(delay);
        showing = false;
        animator.SetBool("showing", false);
        questText.text = "";
    }
}
