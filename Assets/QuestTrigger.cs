using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class QuestTrigger : MonoBehaviour
{
    public QuestTrigger questToComplete;
    public string questText;
    public bool triggered;
    public bool area;
    float questDelay = 1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision & collision.tag == "Player" && !triggered && area)
        {
            TriggerQuest();
        }
    }

    public void TriggerQuest()
    {
        triggered = true;
        if (questToComplete != null)
        {
            questToComplete.CompleteQuest();
            FindObjectOfType<QuestManager>().Show(questText, questDelay * 2);
        }
        else
        {
            FindObjectOfType<QuestManager>().Show(questText, 0);
        }
        //FindObjectOfType<UIAudioManager>().Play("quest_get");
        QuestManager.activeQuest = this;
    }

    public void CompleteQuest()
    {
        if (triggered)
        {
            //FindObjectOfType<UIAudioManager>().Play("quest_complete");
            FindObjectOfType<QuestManager>().Hide(questDelay);
        }
    }
}
